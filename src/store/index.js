import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {},
  },
  mutations: {
    UPDATE_USER(state, user) {
      state.user = user
    },
  },
  actions: {
    updateUser({ commit }, user) {
      commit('UPDATE_USER', user)
    },
  },
})
