/* eslint-disable consistent-return */
/* 

*****************************
*         Request        *
*****************************

¤ fileName  : Request.js
¤ Author    : GUMBAU ELric
¤ Desc      : Sand HTTP requests
¤ Date      : 31/03/2021
¤ Copyright : Klub

*/

/** Import */
import axios from 'axios'

export default function Request() {
  const get = (url, headers) => {
    return new Promise((resolve, reject) => {
      axios
        .get(url, {
          headers,
        })
        .then(response => {
          const { data } = response

          resolve(data)
        })
        .catch(err => {
          reject(new Error(err))
        })
    })
  }
  const post = (url, body) => {
    return new Promise((resolve, reject) => {
      axios
        .post(url, body)
        .then(response => {
          const { status, data } = response

          if (status === 200) resolve(data)
        })
        .catch(err => {
          reject(new Error(err))
        })
    })
  }
  return {
    get,
    post,
  }
}
