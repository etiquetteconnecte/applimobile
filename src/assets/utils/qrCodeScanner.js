/* 

*****************************
*      Qr Code Scanner      *
*****************************

¤ fileName  : qrCodeScanner.js
¤ Author    : Elric GUMBAU
¤ Desc      : scan a qr code with the camera
¤ Date      : 15/01/2021
¤ Copyright : Klub

*/

// Imports
import { QRScanner } from '@ionic-native/qr-scanner/ngx'

const qrScanner = new QRScanner()

export default function qrCodeScanner() {
  // This function allows you to scan a qr code and send its content
  const scanQrCode = () => {
    return new Promise(resolve => {
      qrScanner
        .prepare()
        .then(status => {
          if (status.authorized) {
            // camera permission was granted
            document.getElementsByTagName('body')[0].style.opacity = '0'
            qrScanner.show() // start scanning

            const scanSub = qrScanner.scan().subscribe(text => {
              qrScanner.hide() // hide camera preview
              scanSub.unsubscribe() // stop scanning
              qrScanner.destroy() // destroy scanning
              document.getElementsByTagName('body')[0].style.opacity = '1'
              resolve(text) // Resolve promise
            })
          } else if (status.denied) {
            // camera permission was permanently denied
            // you must use QRScanner.openSettings() method to guide the user to the settings page
            // then they can grant the permission from there
            console.log(status.denied)
          } else {
            // permission was denied, but not permanently. You can ask for permission again at a later time.
            console.log('permission was denied')
          }
        })
        .catch(e => console.log('Error is', e))
    })
  }

  return {
    scanQrCode,
  }
}
