import Vue from 'vue'
import { Notyf } from 'notyf'
import App from './App.vue'
import router from './router'
import store from './store'
import 'notyf/notyf.min.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  provide: () => {
    return {
      notyf: new Notyf({
        duration: 5000, // Set your global Notyf configuration here
        types: [
          {
            type: 'error',
            dismissible: true,
          },
        ],
      }),
    }
  },
  render: h => h(App),
}).$mount('#app')
