# Klub

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Init folder for native app

```
npm run init:ios or npm run init:android
```

### Build native app

```
npm run build:ios or npm run build:android
```
